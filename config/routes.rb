Spree::Core::Engine.routes.draw do
  # Override for Production state
  namespace :admin, path: Spree.admin_path do
    resources :orders, except: [:show] do
      get :production, on: :member
      post :set_production, on: :member
    end

    resource :banner_message_settings, only: [:edit, :update] do
    end
  end
end

Spree::Core::Engine.add_routes do
  get '/category/*id', :to => 'taxons#show', :as => :categories
end
