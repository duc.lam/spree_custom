Spree::Adjustment.class_eval do
  # Override to fix issue:
  # https://github.com/spree/spree/issues/8616
  # https://github.com/spree/spree/pull/8747/commits/5756a6a70f93699aec420bc1895de4fd23e6ad41
  scope :open, -> {
    ActiveSupport::Deprecation.warn 'Adjustment.open is deprecated. Please use Adjustment.not_finalized instead', caller
    not_finalized
  }

  scope :closed, -> {
    ActiveSupport::Deprecation.warn 'Adjustment.closed is deprecated. Please use Adjustment.finalized instead', caller
    finalized
  }

  scope :not_finalized, -> { where(state: 'open') }
  scope :finalized, -> { where(state: 'closed') }
end