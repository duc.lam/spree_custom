Spree::Order.class_eval do
  # For display total in side cart
  money_methods :total_side_cart

  def total_side_cart
    total - tax_total - shipment_total
  end

  # Override to disable order sync with MailChimp which is not working
  def notify_mail_chimp
  end

  # Set by admin/order_controller to be used to log state change for production state
  attr_accessor :current_user

  self.whitelisted_ransackable_attributes << 'production_state'

  # Update production state
  PRODUCTION_STATES = %w(ready backorder processing follow_up_requested production_room fulfillment_room in_qc in_labels complete canceled)

  validates :production_state,       inclusion:    { in: PRODUCTION_STATES, allow_blank: true }
  
  # Hack to get constant
  def self.get_production_states
    const_get("PRODUCTION_STATES")
  end

  # Override to
  # - add customer code to email invoice and packaging list for automatic printing
  # - update production state
  def finalize!
    # lock all adjustments (coupon promotions, etc.)
    all_adjustments.each(&:close)

    # update payment and shipment(s) states, and save
    updater.update_payment_state
    shipments.each do |shipment|
      shipment.update!(self)
      shipment.finalize!
    end

    updater.update_shipment_state

    # update production state
    updater.update_production_state

    save!
    updater.run_hooks

    if ActiveRecord::Base.connection.table_exists?('spree_netsuite_settings') && Spree::NetsuiteSetting.active?
      NetsuiteCreateOrderWorker.perform_async(self.id)
    end

    touch :completed_at

    unless confirmation_delivered?
      deliver_order_confirmation_email
      
      # custom code to email invoice and packaging list for automatic printing
      if Rails.configuration.x.backoffice.print_invoice_packaging_list
        deliver_backoffice_invoice_email
        deliver_backoffice_pick_list_email
      end
    end

    consider_risk
  end

  def deliver_backoffice_invoice_email
    Spree::InvoiceMailer.invoice_email(self).deliver_later
  end

  def deliver_backoffice_pick_list_email
    Spree::InvoiceMailer.pick_list_email(self).deliver_later
  end

  def deliver_backoffice_packing_list_email
    Spree::InvoiceMailer.packing_list_email(self).deliver_later
  end

  def state_changed(name)
    changed_by_user_id = user_id
    if name == 'production' && !current_user.nil?
      changed_by_user_id = current_user.id
    end

    state = "#{name}_state"
    if persisted?
      old_state = send("#{state}_was")
      new_state = send(state)
      unless old_state == new_state
        state_changes.create(
          previous_state: old_state,
          next_state:     new_state,
          name:           name,
          user_id:        changed_by_user_id
        )
      end
    end
  end
end