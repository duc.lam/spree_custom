require_dependency 'spree/shipping_calculator'

module Spree
  module Calculator::Shipping
    class FreeShippingOverAmountCalculator < ShippingCalculator
      preference :amount, :decimal, default: 0
      
      def available?(package)
        package.order.total >= preferred_amount
      end

      def self.description
        Spree.t(:free_shipping_over_amount)
      end

      def compute_package(package)
        0.00
      end
    end
  end
end