require_dependency 'spree/shipping_calculator'

module Spree
  module Calculator::Shipping
    class LocalPickupCalculator < ShippingCalculator
      
      def available?(package)
        true
      end

      def self.description
        Spree.t(:local_pickup)
      end

      def compute_package(package)
        0.00
      end
    end
  end
end