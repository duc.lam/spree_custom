Spree::ShippingMethod.class_eval do

  CODE_FEDEX = 'fedex'
  CODE_UPS = 'ups'
  CODE_USPS = 'usps'
  
  def build_tracking_url(tracking)
    return if tracking.blank?

    return tracking_url.gsub(/:tracking/, ERB::Util.url_encode(tracking)) if !tracking_url.blank? # :url_encode exists in 1.8.7 through 2.1.0

    carrier = deduceTrackingCode(tracking)
    case carrier
    when CODE_FEDEX
      carrier_tracking_url = 'https://www.fedex.com/apps/fedextrack/?action=track&tracknumbers=:tracking'
    when CODE_UPS
      carrier_tracking_url = 'http://wwwapps.ups.com/WebTracking/track?track=yes&trackNums=:tracking'
    when CODE_USPS
      carrier_tracking_url = 'https://tools.usps.com/go/TrackConfirmAction_input?qtc_tLabels1=:tracking'
    else
      carrier_tracking_url = ''
    end

    carrier_tracking_url.gsub(/:tracking/, ERB::Util.url_encode(tracking))
  end

  private
  
  def deduceTrackingCode(tracking_code)
    if tracking_code.scan(/^[0-9]{15}$/).any? || tracking_code.scan(/^[0-9]{12}$/).any?
      CODE_FEDEX
    elsif tracking_code.scan(/^1Z[A-Z0-9]{3}[A-Z0-9]{3}[0-9]{2}[0-9]{4}[0-9]{4}$/i).any? || tracking_code.scan(/^1Z[A-Z0-9]{16}$/i).any?
      CODE_UPS
    elsif tracking_code.scan(/^[0-9]{4}[0-9]{4}[0-9]{4}[0-9]{4}[0-9]{4}[0-9]{2}$/).any?
      CODE_USPS
    else
      ''
    end
  end

end
