Spree::LineItem.class_eval do
  # added lot_numbers
  self.whitelisted_ransackable_attributes = %w[variant_id lot_numbers]
end
