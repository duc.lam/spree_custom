Spree::Adjustable::PromotionAccumulator.class_eval do
  # Override to fix issue:
  # https://github.com/spree/spree/issues/8616
  # https://github.com/spree/spree/pull/8747/commits/5756a6a70f93699aec420bc1895de4fd23e6ad41
  def promotions_adjustments(promotion_id, adjustments = self.adjustments)
    where(sources, promotion_id: promotion_id).map do |source|
      where(adjustments, source_id: source.id)
    end.flatten
  end
end