Spree::Promotion.class_eval do

  def credits
    Spree::Adjustment.eligible.promotion.where(source_id: actions.map(&:id), state: 'closed')
  end

end