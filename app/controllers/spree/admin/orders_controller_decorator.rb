module Spree
  module Admin
    OrdersController.class_eval do

      # Override to fix order search/unchecked show only completed order
      # Add "select" query to include date(created_at) in @search variable
      def index
        params[:q] ||= {}
        params[:q][:completed_at_not_null] ||= '1' if Spree::Config[:show_only_complete_orders_by_default]
        @show_only_completed = params[:q][:completed_at_not_null] == '1'
        params[:q][:s] ||= @show_only_completed ? 'completed_at desc' : 'created_at desc'
        params[:q][:completed_at_not_null] = '' unless @show_only_completed

        # As date params are deleted if @show_only_completed, store
        # the original date so we can restore them into the params
        # after the search
        created_at_gt = params[:q][:created_at_gt]
        created_at_lt = params[:q][:created_at_lt]

        params[:q].delete(:inventory_units_shipment_id_null) if params[:q][:inventory_units_shipment_id_null] == '0'

        if params[:q][:created_at_gt].present?
          params[:q][:created_at_gt] = begin
                                         Time.zone.parse(params[:q][:created_at_gt]).beginning_of_day
                                       rescue StandardError
                                         ''
                                       end
        end

        if params[:q][:created_at_lt].present?
          params[:q][:created_at_lt] = begin
                                         Time.zone.parse(params[:q][:created_at_lt]).end_of_day
                                       rescue StandardError
                                         ''
                                       end
        end

        if @show_only_completed
          params[:q][:completed_at_gt] = params[:q].delete(:created_at_gt)
          params[:q][:completed_at_lt] = params[:q].delete(:created_at_lt)
        end

        @search = Spree::Order.preload(:user).accessible_by(current_ability, :index).ransack(params[:q])

        # lazy loading other models here (via includes) may result in an invalid query
        # e.g. SELECT  DISTINCT DISTINCT "spree_orders".id, "spree_orders"."created_at" AS alias_0 FROM "spree_orders"
        # see https://github.com/spree/spree/pull/3919
        @orders = @search.result(distinct: true).
                  select('spree_orders.*, date(spree_orders.created_at)').
                  page(params[:page]).
                  per(params[:per_page] || Spree::Config[:admin_orders_per_page])

        # Restore dates
        params[:q][:created_at_gt] = created_at_gt
        params[:q][:created_at_lt] = created_at_lt
      end
      
      def production
      end

      def set_production
        last_state = @order.production_state
        @order.production_state = params[:order][:production_state]
        if last_state != @order.production_state
          # Save current user for state change
          @order.current_user = spree_current_user
          @order.state_changed('production')

          if @order.update_attributes(production_state: params[:order][:production_state])
            flash[:success] = flash_message_for(@order, :successfully_updated)
          else
            flash[:error] = @order.errors.full_messages.join(', ')
          end

          if params[:order][:redirect]
            redirect_to params[:order][:redirect]
          else
            redirect_to admin_order_state_changes_url(@order)
          end
        else 
          flash[:error] = Spree.t(:production_state_selected_same_as_previous)

          redirect_to production_admin_order_url(@order)
        end
      end

      # Override to fix issue:
      # https://github.com/spree/spree/issues/8616
      # https://github.com/spree/spree/pull/8747/commits/5756a6a70f93699aec420bc1895de4fd23e6ad41
      def open_adjustments
        adjustments = @order.all_adjustments.finalized
        adjustments.update_all(state: 'open')
        flash[:success] = Spree.t(:all_adjustments_opened)
  
        respond_with(@order) { |format| format.html { redirect_back fallback_location: spree.admin_order_adjustments_url(@order) } }
      end

      def close_adjustments
        adjustments = @order.all_adjustments.not_finalized
        adjustments.update_all(state: 'closed')
        flash[:success] = Spree.t(:all_adjustments_closed)
  
        respond_with(@order) { |format| format.html { redirect_back fallback_location: spree.admin_order_adjustments_url(@order) } }
      end
    end
  end
end