module Spree
  class PagesController < Spree::StoreController

    def show
      locale = I18n.locale

      if valid_page?(locale)
        if locale.nil? || locale == :en
          render template: "spree/pages/#{params[:p]}"
        else
          render template: "spree/pages/#{params[:p]}.#{locale}"
        end
      else
        if locale.nil? || locale == :en
          render file: "public/404.html", status: :not_found, layout: false
        else
          render file: "public/404.#{locale}.html", status: :not_found, layout: false
        end
      end
    end

    private
    def valid_page?(locale)
      if locale.nil? || locale == :en
        template_exists?("spree/pages/#{params[:p]}.html.erb")
      else
        template_exists?("spree/pages/#{params[:p]}.#{locale}.html.erb")
      end
    end
  end
end
