module Spree
  CheckoutController.class_eval do

    def edit
      #session[:cc_number] will be found when coming from payment to confirm
      if @order.confirm?
        payment = @order.payments.checkout.last
        @source = payment.try(:source)
        @payment_method = payment.try(:payment_method)

        if @source.blank?
          @order.state = 'payment'
          @order.save!
          redirect_to(checkout_state_path(@order.state)) && return
        end

        if @source.is_a?(Spree::CreditCard) && !@source.has_payment_profile?
          @source.delete
          payment.delete
        end


        @number = session[:cc_number]
        @verification_value = session[:cc_verification_value]
        @cc_type = session[:cc_type]
        @expiry = session[:cc_expiry]
        #unset info
        session[:cc_number] = ''
        session[:cc_verification_value] = ''
        session[:cc_type] = ''
      end
    end

    def update
      if  params['state'] == 'payment'
        session[:cc_save_checkbox] = params['save_cc'].present?
      end

      if @order.update_from_params(params, permitted_checkout_attributes, request.headers.env)
        @order.temporary_address = !params[:save_user_address]
        unless @order.next
          if @order.confirm?
            state = 'payment'
          end

          state = 'address' if state == nil
          flash[:error] = @order.errors.full_messages.join("\n")
          redirect_to(checkout_state_path(state)) && return
        end

        if @order.completed?
          @current_order = nil
          flash.notice = Spree.t(:order_processed_successfully)
          flash['order_completed'] = true
          redirect_to completion_route
        else
          if @order.confirm?
            source = params["order"]["payments_attributes"].last["source_attributes"]
            session[:cc_number] = source["number"]
            session[:cc_verification_value] = source["verification_value"]
            session[:cc_type] = source["cc_type"]
            session[:cc_expiry] = source["expiry"]
          end

          redirect_to checkout_state_path(@order.state)
        end
      else
          if @order.confirm?
            @order.state = 'payment'
          end
        render :edit
      end
    end

   private

    def skip_state_validation?
      @order.present? && @order.confirm?
    end
  end
end