module Spree
  module Api
    module V1
      ShipmentsController.class_eval do

        def add
          quantity = params[:quantity].to_i
          lotNumbers = params[:lot_numbers]
          lineItemId = params[:line_item_id]
          isAddFromStockLocation = ActiveModel::Type::Boolean.new.cast(params[:is_add_from_stock_location])

          line_item = Spree::Dependencies.cart_add_item_service.constantize.call(order: @shipment.order,
                                                                     variant: variant,
                                                                     quantity: quantity,
                                                                     options: { shipment: @shipment, line_item_id: lineItemId, lot_numbers: lotNumbers, is_add_from_stock_location: isAddFromStockLocation })

          respond_with(@shipment, default_template: :show)
        end

        def remove
          quantity = if params.key?(:quantity)
                       params[:quantity].to_i
                     else
                       @shipment.inventory_units_for(variant).sum(:quantity)
                     end
          lotNumbers = params[:lot_numbers]
          lineItemId = params[:line_item_id]

          Spree::Dependencies.cart_remove_item_service.constantize.call(order: @shipment.order,
                                                                        variant: variant,
                                                                        quantity: quantity,
                                                                        options: { shipment: @shipment, line_item_id: lineItemId, lot_numbers: lotNumbers })

          if @shipment.inventory_units.any?
            @shipment.reload
          else
            @shipment.destroy!
          end

          respond_with(@shipment, default_template: :show)
        end
      end
    end
  end
end
