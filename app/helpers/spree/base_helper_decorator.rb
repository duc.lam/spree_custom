Spree::BaseHelper.module_eval do

    def seo_url(taxon)
      categories_path(taxon.permalink)
    end
    def link_to_cart(text = nil)
      text = text ? h(text) : Spree.t('cart')
      css_class = nil

      order = simple_current_order(true)

      if order.nil? || order.item_count.zero?
        text = "<span data-order-num=\"#{order.number}\" data-order-token=\"#{order.guest_token}\" class=\"glyphicon glyphicon-shopping-cart\"></span> #{text}: (#{Spree.t('empty')})"
        css_class = 'empty'
      else
        text = "<span data-order-num=\"#{order.number}\" data-order-token=\"#{order.guest_token}\" 'class=\"glyphicon glyphicon-shopping-cart\"></span> #{text}: (#{order.item_count})  <span class='amount'>#{order.display_total.to_html}</span>"
        css_class = 'full'
      end

      link_to text.html_safe, spree.cart_path, class: "cart-info #{css_class}"
    end

    def create_product_image_tag(image, product, options, style)
      options[:alt] = image.alt.blank? ? product.name : image.alt
      path = Rails.env.development? ? main_app.url_for(image.url(style)) :  image.my_cf_image_url(style)
      image_tag  path,options
    end

    def sort_shipping_rates_local_pickup_last(shipping_rates)
      shipping_rates.sort_by {|x, y| if x.shipping_method.calculator.class.name == "Spree::Calculator::Shipping::LocalPickupCalculator" then 1 else 0 end }
    end

end
