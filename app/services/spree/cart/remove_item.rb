module Spree
  module Cart
    class RemoveItem
      prepend Spree::ServiceModule::Base

      def call(order:, variant:, quantity: nil, options: nil)
        options ||= {}
        quantity ||= 1

        ActiveRecord::Base.transaction do
          line_item = remove_from_line_item(order: order, variant: variant, quantity: quantity, options: options)
          Spree::Dependencies.cart_recalculate_service.constantize.call(line_item: line_item,
                                                                        order: order,
                                                                        options: options)
          success(line_item)
        end
      end

      private

      def remove_from_line_item(order:, variant:, quantity:, options:)
        if options.key? :line_item_id
          line_item = Spree::LineItem.find_by!(id: options[:line_item_id])
        else
          line_item = Spree::Dependencies.line_item_by_variant_finder.constantize.new.execute(order: order, variant: variant, options: options)
        end

        raise ActiveRecord::RecordNotFound if line_item.nil?

        line_item.quantity -= quantity
        line_item.target_shipment = options[:shipment]

        if line_item.quantity.zero?
          order.line_items.destroy(line_item)
        else
          line_item.lot_numbers = options[:lot_numbers] if options.key? :lot_numbers
          line_item.save!
        end

        line_item
      end
    end
  end
end
