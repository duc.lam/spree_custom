module Spree
  module Cart
    class AddItem
      prepend Spree::ServiceModule::Base

      def call(order:, variant:, quantity: nil, options: {})
        ApplicationRecord.transaction do
          run :add_to_line_item
          run Spree::Dependencies.cart_recalculate_service.constantize
        end
      end

      private

      def add_to_line_item(order:, variant:, quantity: nil, options: {})
        options ||= {}
        quantity ||= 1
        
        is_update = (quantity.to_i == 0 ? false : true)
        
        if options.key? :line_item_id && options[:line_item_id] != ""
          line_item = Spree::LineItem.find_by!(id: options[:line_item_id])
        else
          line_item = Spree::Dependencies.line_item_by_variant_finder.constantize.new.execute(order: order, variant: variant, options: options)
        end
        
        line_item_created = line_item.nil?
        if line_item.nil?
          opts = ::Spree::PermittedAttributes.line_item_attributes.flatten.each_with_object({}) do |attribute, result|
            result[attribute] = options[attribute]
          end.merge(currency: order.currency).delete_if { |_key, value| value.nil? }

          line_item = order.line_items.new(quantity: quantity,
                                           variant: variant,
                                           options: opts)
        else
          line_item.quantity += quantity.to_i
        end

        line_item.target_shipment = options[:shipment] if options.key? :shipment
        
        has_new_lot_numbers = false
        if options.key? :lot_numbers
          if options[:lot_numbers] != line_item.lot_numbers
            has_new_lot_numbers = true
          end
        end
        
        is_add_from_stock_location = false
        if options.key? :is_add_from_stock_location
          is_add_from_stock_location = options[:is_add_from_stock_location]
        end

        if !is_add_from_stock_location && has_new_lot_numbers
          is_update = true
          line_item.lot_numbers = options[:lot_numbers]
        end

        # only update if there are changes
        # otherwise the line item disappear from the UI even though record still exists in db. [TODO: fix this issue]
        if is_update
          return failure(line_item) unless line_item.save
        end
        
        ::Spree::TaxRate.adjust(order, [line_item.reload]) if line_item_created
        success(order: order, line_item: line_item, line_item_created: line_item_created, options: options)
      end
    end
  end
end
