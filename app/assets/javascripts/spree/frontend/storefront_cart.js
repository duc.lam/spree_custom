// NOT USED CURRENTLY. DELETE THIS MESSAGE IF USING. BE SURE TO ADD TO spree_custom.js

//= require spree/api/main

// API routes
Spree.routes.api_v2_storefront_cart_set_quantity = Spree.pathFor('api/v2/storefront/cart/set_quantity')

SpreeAPI.Storefront.cartSetQuantity = function (lineItemId, quantity, successCallback, failureCallback) {
  fetch(Spree.routes.api_v2_storefront_cart_set_quantity, {
    method: 'PATCH',
    headers: SpreeAPI.prepareHeaders({ 'X-Spree-Order-Token': SpreeAPI.orderToken }),
    body: JSON.stringify({
      line_item_id: lineItemId,
      quantity: quantity
    })
  }).then(function (response) {
    switch (response.status) {
      case 422:
        response.json().then(function (json) { failureCallback(json.error) })
        break
      case 500:
        SpreeAPI.handle500error()
        break
      case 200:
        response.json().then(function (json) {
          successCallback(json.data)
        })
        break
    }
  })
}