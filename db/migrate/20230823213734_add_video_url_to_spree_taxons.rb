class AddVideoUrlToSpreeTaxons < ActiveRecord::Migration[5.2]
  def change
    add_column :spree_taxons, :video_url, :string
  end
end
