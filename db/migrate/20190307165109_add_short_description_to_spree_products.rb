class AddShortDescriptionToSpreeProducts < ActiveRecord::Migration[5.2]
  def change
    add_column :spree_products, :short_description, :text
  end
end
