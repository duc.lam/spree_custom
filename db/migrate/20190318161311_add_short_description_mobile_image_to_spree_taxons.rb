class AddShortDescriptionMobileImageToSpreeTaxons < ActiveRecord::Migration[5.2]
  def change
    add_column :spree_taxons, :short_description_mobile_image, :string
  end
end
