class AddNoindexToSpreeTaxons < ActiveRecord::Migration[5.2]
  def change
    add_column :spree_taxons, :noindex, :boolean, default:false
  end
end
