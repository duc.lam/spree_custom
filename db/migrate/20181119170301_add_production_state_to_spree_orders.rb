class AddProductionStateToSpreeOrders < ActiveRecord::Migration[5.2]
  def change
    add_column :spree_orders, :production_state, :string
  end
end
