class AddLotNumbersToSpreeLineItems < ActiveRecord::Migration[5.2]
  def change
    add_column :spree_line_items, :lot_numbers, :string
  end
end
