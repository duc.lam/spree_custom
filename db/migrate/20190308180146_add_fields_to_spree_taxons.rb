class AddFieldsToSpreeTaxons < ActiveRecord::Migration[5.2]
  def change
    add_column :spree_taxons, :short_description, :text
    add_column :spree_taxons, :short_description_image, :string
    add_column :spree_taxons, :description_image, :string
    add_column :spree_taxons, :directions, :text
    add_column :spree_taxons, :nutritional_panel_image, :string
  end
end
